package com.etrainu.xml

interface XMLSerializable {
	String toXML()
	void toXMLNode(groovy.xml.MarkupBuilder builder)
}
