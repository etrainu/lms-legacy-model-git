package com.etrainu.legacy.model

import grails.test.*

import com.etrainu.legacy.model.referrer.Referrer

class ReferrerTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

	void testConstraints() {
		def testReferrer = new Referrer(id: "test.com.au", name: "my test referrer")
		mockForConstraintsTests(Referrer, [ testReferrer ])
		
		// Validation should fail if we do no set id or name.
		def referrer = new Referrer()
		assertFalse referrer.validate()
		assertEquals "nullable", referrer.errors["name"]
		assertEquals "nullable", referrer.errors["id"]
		
		// So let's demonstrate the unique and minSize constraints.
		def nonUniqueReferrer = new Referrer(id: "", name: "my test referrer")
		assertFalse nonUniqueReferrer.validate()
		assertEquals "unique", 	nonUniqueReferrer.errors["name"]
		//assertEquals "minSize", book.errors["author"]

		// Validation should pass!
		def valideReferrer = new Referrer(id: "Valid", name: "unique name")
		assertTrue valideReferrer.validate()
	}
	
    void testMappings() 
	{
		def testReferrer = new Referrer(id: "test.com.au", name: "my test referrer", isDiscount:true, discount:20.75)
		mockDomain(Referrer, [ testReferrer ])
		
		assertNull Referrer.get("test.co.nz")
		assert Referrer.get("test.com.au") 
		assertEquals testReferrer.discount, Referrer.get("test.com.au").discount
		assertEquals testReferrer.name, Referrer.get("test.com.au").name
		assertEquals testReferrer.id, Referrer.get("test.com.au").id
		assertNull Referrer.get("test.com.au").archiveDate
		assertFalse Referrer.get("test.com.au").isPercentage
		assertTrue Referrer.get("test.com.au").isDiscount
		
    }
}
