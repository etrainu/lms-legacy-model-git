dataSource {
	pooled = true
	driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
	username = "sa"
	password = "eTnh3Bu&1"
	logSql = true
}
hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = true
	cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}
// environment specific settings
environments {
	development {
		dataSource {
			//Commenting our dbCreate to instruct Grails to NOT update legacy DB
			//dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			url = "jdbc:sqlserver://cn-app02:1433;databaseName=etrainu"
		}
	}
	test {
		dataSource {
			//Commenting our dbCreate to instruct Grails to NOT update legacy DB
			//dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			logSql = true
			url = "jdbc:sqlserver://cn-app02:1433;databaseName=etrainu"
		}
	}
	production {
		dataSource {
			//Commenting our dbCreate to instruct Grails to NOT update legacy DB
			dbCreate = "none" // one of 'create', 'create-drop','update'
			url = "jdbc:sqlserver://10.81.170.4:1433;databaseName=etrainu"
			username = 'db_restrictedUser'
			password = 'QUz-$2wr'
			}
	}
	steele {
		dataSource {
			url = "jdbc:sqlserver://webdev3:1433;databaseName=etrainu"
			password = "Australia1!"
			logSql = false
		}
	}
}
