package com.etrainu.legacy.model.training

class CourseInCategory implements Serializable {
	static mapping = {
		table 'view_CourseInCategory'
		version false
		id composite: ["categoryId","inductionId"]
		columns {
			categoryId column: "categoryId"
			inductionId column: "inductionId"
			parentCategoryId column: "parentCategoryId"
		}
	}
	
	String categoryId
	String inductionId
	String parentCategoryId
}
