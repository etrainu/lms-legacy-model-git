package com.etrainu.legacy.model.training

class Category {

    static constraints = {
    }
	
	static mapping = {
		table "tbl_courseCategory"
		version false
		columns {
		  id column: "categoryID"
		  name column: "name"
		  isPublic column: "isPublic"
		}
	  }
	  
	  String id
	  String name
	  boolean isPublic
}
