package com.etrainu.legacy.model.training

import com.etrainu.xml.XMLSerializable

class CourseDetails implements XMLSerializable {
	static mapping = {
		table 'view_inductionData'
		version false
		id generator: "guid"
		columns {
			id column: "inductionID"
			inductionName column: "inductionName"
			courseCode column: "courseCode"
			isStandalone column: "isStandalone"
			isScorm column: "isScorm"
			feedbackRequired column: "feedbackRequired"
			cost column: "cost"
			freeKey column: "freeKey"
			autoActivate column: "autoActivate"
			autoMarkCompetent column: "autoMarkCompetent"
			participantCount column: "participantCount"
			completedCount column: "completedCount"
			deactivatedCount column: "deactivatedCount"
			expiredCount column: "expiredCount"
			certificateCount column: "certificateCount"
			averageCompletionDays column: "averageCompletionDays"
			averageCompletionMinutes column: "averageCompletionMinutes"
			firstEnrolment column: "firstEnrolment"
			lastEnrolment column: "lastEnrolment"
			categoryCount column: "categoryCount"
			customPricing column: "customPricing"
			bundleCount column: "bundleCount"
			trainingPlanCount column: "trainingPlanCount"
			searchData column: "searchData"
			archiveDate column: "archiveDate"
			supplier column: "supplier"
		}
	}

	String id
	String inductionName
	String courseCode
	Boolean isStandalone
	Boolean isScorm
	Boolean feedbackRequired
	Double cost
	Boolean freeKey
	Boolean autoActivate
	Boolean autoMarkCompetent
	Integer participantCount
	Integer completedCount
	Integer deactivatedCount
	Integer expiredCount
	Integer certificateCount
	Integer averageCompletionDays
	Integer averageCompletionMinutes
	Date firstEnrolment
	Date lastEnrolment
	Integer categoryCount
	Boolean customPricing
	Integer bundleCount
	Integer trainingPlanCount
	String searchData
	Date archiveDate
	String supplier

	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}

	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.course(id:this.id)  {
			id(this.id)
			inductionName(this.inductionName)
			courseCode(this.courseCode)
			isStandalone(this.isStandalone)
			isScorm(this.isScorm)
			feedbackRequired(this.feedbackRequired)
			cost(this.cost)
			freeKey(this.freeKey)
			autoActivate(this.autoActivate)
			autoMarkCompetent(this.autoMarkCompetent)
			participantCount(this.participantCount)
			completedCount(this.completedCount)
			deactivatedCount(this.deactivatedCount)
			expiredCount(this.expiredCount)
			certificateCount(this.certificateCount)
			averageCompletionDays(this.averageCompletionDays)
			averageCompletionMinutes(this.averageCompletionMinutes)
			if( this.firstEnrolment ) {
				firstEnrolment(this.firstEnrolment.format("yyyy-M-dd"))
			} else {
				firstEnrolment()
			}
			if( this.lastEnrolment ) {
				lastEnrolment(this.lastEnrolment.format("yyyy-M-dd"))
			} else {
				lastEnrolment()
			}
			categoryCount(this.categoryCount)
			customPricing(this.customPricing)
			bundleCount(this.bundleCount)
			trainingPlanCount(this.trainingPlanCount)
			searchData(this.searchData)
			if( this.archiveDate ) {
				archiveDate(this.archiveDate.format("yyyy-M-dd"))
			} else {
				archiveDate()
			}
			supplier(this.supplier)
		}
	}
}
