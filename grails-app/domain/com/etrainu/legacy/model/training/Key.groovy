package com.etrainu.legacy.model.training


class Key {

   static constraints = {
    }
	
	
	static mapping = {
		table "view_UserInductionStatus"
		version false
		columns {
		  id column: "inductionKey"
		  userId column: "UserID"
		  course column: "InductionID"
		  status column: "status"
		}
	  }
	  
	  String id
	  String userId
	  Course course
	  String status
}
