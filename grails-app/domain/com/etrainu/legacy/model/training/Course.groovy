package com.etrainu.legacy.model.training

import com.etrainu.xml.XMLSerializable

class Course implements XMLSerializable  {

	static constraints = { 
		archiveDate(nullable:true) 
		desc(nullable:true)
	}

	static mapping = {
		table "tbl_induction"
		version false
		columns {
			id column: "inductionId"
			name column: "inductionName"
			desc column : "inductionDescription"
			cost colum : "cost"
			archiveDate column: "archiveDate"
		}
		cache include:'non-lazy'
	}

	String id
	String name
	String desc
	Double cost
	Date archiveDate

	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}

	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.course(id:this.id) {
			id(this.id)
			name(this.name)
			desc(this.desc)
			cost(this.cost)
			if( this.archiveDate ) {
				archiveDate(this.archiveDate.format("yyyy-M-dd") )
			} else {
				archiveDate()
			}
		}
	}
}
