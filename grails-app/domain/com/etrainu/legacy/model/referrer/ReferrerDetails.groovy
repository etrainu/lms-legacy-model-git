package com.etrainu.legacy.model.referrer

import com.etrainu.xml.XMLSerializable;

class ReferrerDetails implements XMLSerializable {
	static constraints = {
	}
	
	static mapping = {
		table "view_referrerData"
		version false
		columns {
			id column: "domain"
			name column: "displayName"
			discount column : "discountAmount"
			isDiscount column: "isDiscount"
			isPercentage column: "isPercentage"
			archiveDate column: "archiveDate"
			totalPurchase column: "totalPurchase"
			totalAmount column: "totalAmount"
			firstPurchase column: "firstPurchase"
			lastPurchase column: "lastPurchase"
		}
	}
	
	String id
	String name
	Double discount
	Boolean isDiscount
	Boolean isPercentage
	Date archiveDate
	Integer totalPurchase
	Double totalAmount
	Date firstPurchase
	Date lastPurchase

	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}
	
	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.referrer(id:this.id) {
			domain(this.id)
			name(this.name)
			discount(this.discount)
			isDiscount(this.isDiscount)
			isPercentage(this.isPercentage)
			archiveDate(this.archiveDate)
			totalPurchase(this.totalPurchase)
			totalAmount(this.totalAmount)
			firstPurchase(this.firstPurchase)
			lastPurchase(this.lastPurchase)
		}
	}

}
