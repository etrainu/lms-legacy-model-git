package com.etrainu.legacy.model.referrer

import java.util.Date;

class Transaction {
	
	static constraints = {		
		debitDate(nullable:false)		
		cost(nullable:true)
		gst(nullable:true)
		amount(nullable:true)
		referrerDomain(nullable:true)
		promoCode(nullable:true)
		inductionId(nullable:false)
		purchasedByUserID(nullable:true)
	}
	
	static mapping = {
		table "tbl_transactionDebit"
		version false
		columns {
		  id column: "transactionDebitID"
		  debitDate column: "debitDate"
		  cost column: "cost"
		  gst column: "GST"
		  amount column: "amount"
		  referrerDomain column: "fk_referrerDomain"
		  promoCode column: "fk_promocode"
		  inductionId column: "inductionID"
		  purchasedByUserID column: "purchasedByUserID"
		}
	  }
	  
	  long id
	  Date debitDate	  
	  Double cost
	  Double gst
	  Double amount	  
	  String referrerDomain 
	  String promoCode
	  String inductionId
	  String purchasedByUserID
}
