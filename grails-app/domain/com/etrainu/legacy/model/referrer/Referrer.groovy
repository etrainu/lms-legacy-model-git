package com.etrainu.legacy.model.referrer

import java.util.Date;
import java.text.DateFormat
import java.text.SimpleDateFormat

import com.etrainu.xml.XMLSerializable

class Referrer implements XMLSerializable {

    static constraints = {
		discount(nullable:true, maxsize:50)
		name(unique:true, maxsize:150, nullable:false)
		isDiscount(nullable:false)
		isPercentage(nullable:true)
		archiveDate(nullable:true)
		id(nullable:false)
    }
	
	static mapping = {
		table "tbl_validReferrers"
		id generator: 'assigned'
		version false
		columns {
			id column: "domain"
			name column: "displayName"
			discount column : "discountAmount"			
			isDiscount column: "isDiscount"
			isPercentage column: "isPercentage"
			archiveDate column: "archiveDate"
		}
	}
	
	String id
	String name
	Double discount
	Boolean isDiscount = false
	Boolean isPercentage = false
	Date archiveDate

	String toString() {
		"${name} - ${id}"
	}
	
	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}
	
	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.referrer(id:this.id) {
			domain(this.id)
			name(this.name)
			discount(this.discount)
			isDiscount(this.isDiscount)
			isPercentage(this.isPercentage)
			archiveDate(this.archiveDate)
		}
	}
	
	String update(Object xml) {	
		DateFormat dformat = new SimpleDateFormat("yyyy-M-dd")
		
		if(xml.domain.text())		this.id = xml.domain.text()
		if(xml.@id.text())			this.id = xml.@id.text()
		if(xml.name.text())			this.name = xml.name.text()
		if(xml.discount.text())		this.discount = Double.parseDouble(xml.discount.text())
		if(xml.isDiscount.text())	this.isDiscount = xml.isDiscount.text().toBoolean()
		if(xml.isPercentage.text())	this.isPercentage = xml.isPercentage.text().toBoolean()
		
		if(xml.archiveDate.text()) {
			String archiveData = xml.archiveDate.text()
			
			this.archiveDate = dformat.parse(archiveData)
		}
	}
	
	//Apparently my previous method of defaulting has not worked because the db will retrieve null into the values
	def beforeValidate() {
		if( this.isDiscount == null ) this.isDiscount = false
		if( this.isPercentage == null ) this.isPercentage = false
	}
}
