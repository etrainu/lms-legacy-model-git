package com.etrainu.legacy.model.user

class UserScormAssessment {

    static constraints = {
    }
	
	static mapping = {
		table "tbl_scormAssessment"
		version false
		columns {
		  id column: "scormAssessmentId"
		  userId column: "fk_userId"
		  inductionKey column: "fk_inductionKey"
		  entryDate column: "entryDate"
		}
	  }
	
	String id
	String userId
	String inductionKey
	Date entryDate

}
