package com.etrainu.legacy.model.user

import java.util.Date
import com.etrainu.xml.XMLSerializable

class UserDetails implements XMLSerializable {

	static mapping = {
		table 'view_userData'
		version false
		id generator: "guid"
		columns {
			id column: "userID"
			username column: "username"
			firstname column: "firstname"
			lastname column: "lastname"
			email column: "email"
			groupId column: "fk_groupID"
			participantGroupId column: "participantGroupId"
			lastLogin column: "LastLogin"
			groupData column: "GroupData"
			searchData column: "SearchData"
			loginCount12Months column: "loginCount12Months"
			archiveDate column: "archiveDate"
			city column: "city"
			state column: "state"
			courseCount column: "courseCount"
			firstCourse column: "firstCourse"
			mostRecentCourse column: "mostRecentCourse"
			hierarchy column: "hierarchy"
			completedCourses column: "completedCourses"
			dateCreated column: "dateCreated"
			balance column: "balance"
		}
	}
	
	String id
	String username
	String firstname
	String lastname
	String email
	Integer groupId
	Integer participantGroupId
	Date archiveDate
	Date lastLogin
	Integer loginCount12Months
	String groupData
	String searchData
	String city
	String state
	Integer courseCount
	Date firstCourse
	Date mostRecentCourse
	String hierarchy
	Integer completedCourses
	Date dateCreated
	Float balance
	
   transient beforeInsert = {
      throw new RuntimeException('Create not allowed')
   }

   transient beforeUpdate = {
      throw new RuntimeException('Update not allowed')
   }

   transient beforeDelete = {
      throw new RuntimeException('Delete not allowed')
   } 

	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}

	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.user(id:this.id) {
			username(this.username)
			firstname(this.firstname)
			lastname(this.lastname)
			groupId(this.groupId)
			participantGroupId(this.participantGroupId)
			id(this.id)
			email(this.email)
			if( this.lastLogin ) {
				lastLogin(this.lastLogin.format("yyyy-M-dd") )
			} else {
				lastLogin()
			}
			loginCount12Months(this.loginCount12Months)
			archiveDate(this.archiveDate)
			city(this.city)
			state(this.state)
			courseCount(this.courseCount)
			if( this.firstCourse ) {
				firstCourse(this.firstCourse.format("yyyy-M-dd") )
			} else {
				firstCourse()
			}
			if( this.mostRecentCourse ) {
				mostRecentCourse(this.mostRecentCourse.format("yyyy-M-dd") )
			} else {
				mostRecentCourse()
			}
			hierarchy(this.hierarchy)
			completedCourses(this.completedCourses)
			if( this.dateCreated ) {
				dateCreated(this.dateCreated.format("yyyy-M-dd"))
			} else {
				dateCreated()
			}
			balance(this.balance)
		}
	}
}
