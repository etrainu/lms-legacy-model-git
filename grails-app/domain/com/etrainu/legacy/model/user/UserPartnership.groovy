package com.etrainu.legacy.model.user

import java.io.Serializable

import com.etrainu.xml.XMLSerializable;

class UserPartnership implements XMLSerializable {

	static constraints = {
	}
	
	static mapping = {
		table "view_ParticipantTrainingPartnerships"
		version false
		columns {
		  id column: "participantGroupID"
		  userId column: "userId"
		  userName column: "userName"
		  firstName column: "firstName"
		  lastName column: "lastName"
		  groupId column: "groupID"
		  groupName column: "groupName"
		  middleGroupId column: "middleGroupID"
		  middleGroupName column: "middleGroupName"
		  primaryGroupId column: "primaryGroupID"
		  primaryGroupName column: "primaryGroupName"
		  acceptedByUser column: "acceptedByUser"
		  acceptedByGroup column: "acceptedByGroup"
		}
	}
	
	Integer id
	String userId
	String userName
	String firstName
	String lastName
	Integer groupId
	String groupName
	Integer middleGroupId
	String middleGroupName
	Integer primaryGroupId
	String primaryGroupName
	Integer acceptedByUser
	Integer acceptedByGroup
	
	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}
	
	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.partnership(id:this.id.toString()) {
			id(this.id.toString())
			userId(this.userId)
			userName(this.userName)
			firstName(this.firstName)
			lastName(this.lastName)
			groupId(this.groupId.toString())
			groupName(this.groupName)
			middleGroupId(this.middleGroupId.toString())
			middleGroupName(this.middleGroupName)
			primaryGroupId(this.primaryGroupId.toString())
			primaryGroupName(this.primaryGroupName)
			acceptedByUser(this.acceptedByUser.toString())
			acceptedByGroup(this.acceptedByGroup.toString())
		}
	}	
}
