package com.etrainu.legacy.model.user

class UserLogging implements Serializable {

    static constraints = {
    }
		
	static mapping = {
		table "tbl_userlog"
		version false
		id composite:['userId','logDate'],generator:"assigned"
		columns {
		  userId column: "FK_log_userID"
		  logDate column: "logDate"
		  logIp column: "logIp"
		  logBrowser column: "logBrowser"
		}
	  }
		
	  String userId
	  Date logDate
	  String logIp
	  String logBrowser
}
