package com.etrainu.legacy.model.user

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

import com.etrainu.xml.XMLSerializable

class User implements XMLSerializable {

	def public static final enum UserType {
		ADMIN,
		B2B, //EG Harvey Norman Participant
		B2C //ONLINE Participant, such as RSG
	}

	static transients = ["type"]

	static constraints = {
		street(nullable:true)
		city(nullable:true)
		postcode(nullable:true)
		phone(nullable:true)
		mobile(nullable:true)
		fax(nullable:true)
		dob(nullable:true)
		archiveDate(nullable:true)
		permissions(nullable:true)
		authKey(nullable:true)
		authKeyLastUsed(nullable:true)
	}

	static mapping = {
		table "tbl_user"
		version false
		id generator: "guid"
		columns {
			id column: "userID"
			username column: "username"
			password column: "password"
			firstname column: "firstname"
			lastname column: "lastname"
			email column: "email"
			street column: "street"
			city column: "city"
			postcode column: "postcode"
			mobile column: "mobile"
			fax column: "fax"
			dob column: "dob"
			active column: "active"
			acceptedTerms column: "acceptedTsAndCs"
			canArchive column: "canArchive"
			hideAds column: "hideAds"
			groupId column: "fk_groupID"
			archiveDate column: "archiveDate"
			permissions column: "permissions"
			authKey column: "authKey"
			authKeyLastUsed column: "authKeyLastUsed"
		}
	}

	String id
	String username
	String password
	String firstname
	String lastname
	String email
	String street
	String city
	String postcode
	String phone
	String mobile
	String fax
	Date dob
	Boolean active
	Boolean acceptedTerms
	Boolean canArchive
	Boolean hideAds
	Integer groupId
	Date archiveDate
	String permissions
	String authKey
	Date authKeyLastUsed

	String toString(){
		"${username} - ${firstname} ${lastname}"
	}
	
	
	//Every time a authKey is fetched, it should be changed, because it should only ever be fetched once
	String generateAuthKey() {
		this.authKey = java.util.UUID.randomUUID()
		this.authKeyLastUsed = Calendar.getInstance().time
		this.save()
		
		return this.authKey
	}

	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}

	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		toXMLNode(builder, false)
	}

	void toXMLNode(groovy.xml.MarkupBuilder builder, Boolean simplified) {
		builder.user(id:this.id) {
			username(this.username)
			firstname(this.firstname)
			lastname(this.lastname)
			if( !simplified) password( this.password )
			id(this.id)
			email(this.email)
			if( !simplified ) street(this.street)
			if( !simplified ) city(this.city)
			if( !simplified ) postcode(this.postcode)
			if( !simplified ) phone(this.phone)
			if( !simplified ) mobile(this.mobile)
			if( !simplified ) fax(this.fax)
			if( !simplified ) {
				if( this.dob ) {
					dob( this.dob.format("yyyy-M-dd") )
				} else {
					dob()
				}
			}
			if( !simplified ) active( this.active )
			if( !simplified ) acceptedTerms( this.acceptedTerms )
			if( !simplified ) canArchive( this.canArchive )
			if( !simplified ) hideAds( this.hideAds )
			if( !simplified ) groupId( this.groupId )
			archiveDate( this.archiveDate )
			permissions( this.permissions )
		}
	}

	String update(Object xml) {
		if( xml.password.text() ) 	this.password = xml.password.text()
		if( xml.firstname.text() )	this.firstname = xml.firstname.text()
		if( xml.lastname.text() ) 	this.lastname = xml.lastname.text()
		if( xml.password.text() ) 	this.password = xml.password.text()
		if( xml.email.text() )		this.email = xml.email.text()
		if( xml.street.text() )		this.street = xml.street.text()
		if( xml.city.text() )		this.city = xml.city.text()
		if( xml.postcode.text() )	this.postcode = xml.postcode.text()
		if( xml.phone.text() )		this.phone = xml.phone.text()
		if( xml.mobile.text() )		this.mobile = xml.mobile.text()
		if( xml.fax.text() )		this.fax = xml.fax.text()
		if( xml.active.text() )		this.active = xml.active.text()
		if( xml.canArchive.text() )	this.canArchive = xml.canArchive.text()
		if( xml.hideAds.text() )	this.hideAds = xml.hideAds.text()
		if( xml.groupId.text() )	this.groupId = Integer.parseInt( xml.groupId.text() )
		if( xml.acceptedTerms.text() )
			this.acceptedTerms = xml.acceptedTerms.text()
			
		DateFormat df = new SimpleDateFormat("yyyy-M-dd")
		if( xml.dob.text() ) {
			String dobData = xml.dob.text()

			this.dob = df.parse( dobData )
		}
		if( xml.archiveDate.text() ) {
			String archiveData = xml.archiveDate.text()
			
			this.archiveDate = df.parse( archiveData )
		}
	}
}
