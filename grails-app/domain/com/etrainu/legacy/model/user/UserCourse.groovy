package com.etrainu.legacy.model.user

class UserCourse {

   static constraints = {
    }
	
	static mapping = {
		table "view_UserInduction"
		version false
		columns {
		  id column: "inductionKey"
		  courseId column: "inductionID"
		  userId column: "UserID"
		  status column: "status"
		  courseCode column: "courseCode"
		  providerGroupName column : "providerGroupName"
		  providerGroupId column : "providerGroupId"
		  inductionName column : "inductionName"
		  accessAllowedDate column: "accessAllowedDate"
		  expiryDays column: "expiryDays"
		  createDate column: "createDate"
		  inductionCompetentDate column: "inductionCompetentDate"
		}
	  }
	  
	  String id
	  String courseId
	  String userId
	  String status
	  String courseCode
	  String providerGroupName
	  String providerGroupId
	  String inductionName
	  Date accessAllowedDate
	  Integer expiryDays
	  Date createDate
	  Date inductionCompetentDate
}
