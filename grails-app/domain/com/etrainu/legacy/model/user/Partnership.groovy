package com.etrainu.legacy.model.user

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.exceptions.RequiredPropertyMissingException

import com.etrainu.xml.XMLSerializable

class Partnership implements XMLSerializable {
	
	//Used for parsing between dates and strings
	DateFormat df = new SimpleDateFormat("yyyy-M-dd")
	
	static transients = ['df']
	
	static constraints = {
		archiveDate(nullable:true)
		acceptedDate(nullable:true)
		createdBy(nullable:true)
		createdByUserId(nullable:true)
	}
	
	static mapping = {
		table "tbl_participantGroup"
		version false
		columns {
			id column: "participantGroupID"
			userId column: "fk_userID"
			groupId column: "fk_groupID"
			acceptedByUser column: "acceptedByUser"
			acceptedByGroup column: "acceptedByGroup"
			viewCerts column: "viewCerts"
			viewContactDetails column: "viewContactDets"
			viewInductionDetails column: "viewInductionDets"
			createdBy column: "createdBy"
			createdByUserId column: "fk_adminID"
			viewUserRecords column: "viewUserRecords"
			viewTrainingPlans column: "viewTrainingPlans"
			archiveDate column: "dateSevered"
			acceptedDate column: "dateAccepted"
		}
	}
	
	Integer id
	String userId
	Integer groupId
	Boolean acceptedByUser
	Boolean acceptedByGroup
	Boolean viewCerts
	Boolean viewContactDetails
	Boolean viewInductionDetails
	String createdBy
	String createdByUserId
	Boolean viewUserRecords
	Boolean viewTrainingPlans
	Date archiveDate
	Date acceptedDate
	
	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}
	
	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		def archiveText = (this.archiveDate) ? this.archiveDate.format('yyyy-mm-dd') : '';
		def acceptedText = (this.acceptedDate) ? this.acceptedDate.format('yyyy-mm-dd') : '';
		
		builder.partnership(id:this.id) {
			id(this.id)
			userId(this.userId)
			groupId(this.groupId.toString())
			acceptedByUser(this.acceptedByUser.toString())
			acceptedByGroup(this.acceptedByGroup.toString())
			viewCerts(this.viewCerts.toString())
			viewContactDetails(this.viewContactDetails.toString())
			viewInductionDetails(this.viewInductionDetails.toString())
			createdBy(this.createdBy)
			createdByUserId(this.createdByUserId)
			viewUserRecords(this.viewUserRecords.toString())
			viewTrainingPlans(this.viewTrainingPlans.toString())
			archiveDate(archiveText)
			acceptedDate(acceptedText)
		}
	}
	
	// TODO: Consider moving this and the overloaded method into a class for reverse xml serialization as this method is now very generic.
	// Have this class store the property list below in a property registry of some sort, such as
	// Partnership.xmlProperties [ 'userId', 'groupId' ];
	void update(Object xml) {
		[
			'userId',
			'groupId',
			'acceptedByUser',
			'acceptedByGroup',
			'viewCerts',
			'viewContactDetails',
			'viewInductionDetails',
			'createdBy',
			'createdByUserId',
			'viewUserRecords',
			'viewTrainingPlans',
			'archiveDate',
			'acceptedDate'
		].each { prop ->
			update(xml, prop);
		}
		
		return
	}
	
	void update(Object xml, String property) {
		if( xml[property].text() ) {
			def val = xml[property].text()
			if( this[property] instanceof String ) {
				this[property] = val
				return;
			}
			
			if( this[property] instanceof Integer ) {
				this[property] = Integer.parseInt( val )
				return;
			}
			
			if( this[property] instanceof Boolean ) {
				this[property] = val.toBoolean()
				return;
			}
			
			if( this[property] instanceof Date ) {
				this[property] = df.parse( val )
				return;
			}
			
			throw new RequiredPropertyMissingException("No handler implemented for" + this[property].getClass() + '. Could not parse ' + property + '.')
		}
	}
}
