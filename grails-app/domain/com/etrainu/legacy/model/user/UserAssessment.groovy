package com.etrainu.legacy.model.user

class UserAssessment {

    static constraints = {
    }
	
	static mapping = {
		table "tbl_userAssessment"
		version false
		columns {
		  id column: "userAssessmentID"
		  userId column: "fk_userID"
		  inductionKey column: "fk_inductionKey"
		  isLocked column: "isLocked"
		  isProficient column: "isProficient"
		  isCompleted column: "isCompleted"
		  attempt column: "attempt"
		  dtStarted column: "dtStarted"
		  dtCompleted column: "dtCompleted"
		  totalLockouts column : "totalLockouts"
		  fk_assessorUserID column : "fk_assessorUserID"
		}
	  }
	
	String id
	String userId
	String inductionKey
	Boolean isLocked
	Boolean isProficient
	Boolean isCompleted
	Integer attempt
	Date dtStarted
	Date dtCompleted
	Integer totalLockouts
	String fk_assessorUserID
	

}
