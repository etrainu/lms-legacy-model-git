package com.etrainu.legacy.model.group

class Group {
	static constraints = {
	}
	
	static mapping = {
		table "tbl_userGroup"
		version false
		columns {
		  id column: "groupID"
		  fk_groupTypeId column: "fk_groupTypeID"
		  parentGroup column: "parentGroup"
		  groupCode column: "groupCode"
		  groupName column: "groupName"
		  expiryDate column: "expiryDate"
		  active column: "active"
		  archiveDate column: "archiveDate"
		  isRTO column: "isRTO"
		  fk_suborgGroupingId column: "fk_suborgGroupingID"
		}
	}
	
	Integer id
	Integer fk_groupTypeId
	Integer parentGroup
	String groupCode
	String groupName
	Date expiryDate
	Integer active
	Date archiveDate
	Integer isRTO
	Integer fk_suborgGroupingId
	
}
